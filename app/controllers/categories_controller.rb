class CategoriesController < ApplicationController
	def index
		@categories = Category.all
	end

	def show
		@category = Category.find(params[:id])
	end

	def secret_soups
		session[:vip] = true
		redirect_to root_path
	end
end
